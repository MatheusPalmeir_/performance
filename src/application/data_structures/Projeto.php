<?php 

class Projeto{
    private $id;
    private $nome;
    private $descricao;
    private $imagens;
    private $data;

    public function __construct(){
        $args = func_get_args();
        switch (func_num_args()) {
            case 4:
                self::__construct1($args[0],
                                   $args[1],
                                   $args[2], 
                                   $args[3]);
                break;
            case 5:
                self::__construct2($args[0],
                                   $args[1],
                                   $args[2], 
                                   $args[3], 
                                   $args[4]);
                break;
        }
    }

    public function __construct1(string $nome, 
                                 string $descricao, 
                                 array  $imagens, 
                                 string $data){
        $this->id = self::NO_ID;
        $this->nome = $nome;
        $this->descricao = $descricao;
        $this->imagens = $imagens;
        $this->data = $data;
    }

    public function __construct2(int    $id,
                                 string $nome, 
                                 string $descricao, 
                                 array  $imagens, 
                                 string $data){
        $this->id = $id;
        $this->nome = $nome;
        $this->descricao = $descricao;
        $this->imagens = $imagens;
        $this->data = $data;
    }

    public function getID(){ return $this->id; }
    public function getNome(){ return $this->nome; }
    public function getDescricao(){ return $this->descricao; }
    public function getImagens(){ return $this->imagens; } 
    public function getData(){ return $this->data; } 
}

?>