<?php 

require_once(APPPATH."models/Usuario_model.php");
require_once(APPPATH."data_structures/Imagem.php");

class Usuario{
    private $id;
    private $nome;
    private $email;
    private $senha;
    private $tipo_usuario;
    private $imagem;
    private $data_registro;

    public function __construct(){
        $args = func_get_args();
        switch(func_num_args()){
            case 6:
                self::__construct1($args[0],
                                   $args[1],
                                   $args[2],
                                   $args[3],
                                   $args[4],
                                   $args[5]);
                break;
            case 7:
                self::__construct2($args[0],
                                   $args[1],
                                   $args[2],
                                   $args[3],
                                   $args[4],
                                   $args[5],
                                   $args[6]);
        }
    }

    public function __constructLogin(int    $id,
                                     string $nome,
                                     string $email,
                                     int $tipo_usuario,
                                     Imagem $imagem){
        $this->id = self::NO_ID;
        $this->email = $email;
        $this->nome = $nome;
        $this->tipo_usuario = $tipo_usuario;
        $this->imagem = $imagem;
    }

    public function __construct1(string $email, 
                                 string $senha, 
                                 string $nome,
                                 string $data_registro,
                                 int    $tipo_usuario, 
                                 Imagem $imagem){
        $this->id = self::NO_ID;
        $this->email = $email;
        $this->senha = $senha;
        $this->nome = $nome;
        $this->data_registro = $data_registro;
        $this->tipo_usuario = $tipo_usuario;
        $this->imagem = $imagem;
    }

    public function __construct2(int $id,
                                 string $email, 
                                 string $senha, 
                                 string $nome,
                                 string $data_registro,
                                 int    $tipo_usuario, 
                                 Imagem $imagem){
        $this->id = $id;
        $this->email = $email;
        $this->senha = $senha;
        $this->nome = $nome;
        $this->data_registro = $data_registro;
        $this->tipo_usuario = $tipo_usuario;
        $this->imagem = $imagem;
    }

    public function getID(){ return $this->id; }
    public function getNome(){ return $this->nome; }
    public function getEmail(){ return $this->email; }
    public function getSenha(){ return $this->senha; }
    public function getTipoUsuario(){ return $this->tipo_usuario; }
    public function getImagem(){ return $this->imagem; }
    public function getDataRegistro(){ return $this->data_registro; }
  
    public function serializeSQL(){
        $serialized = array();
        if($this->id != self::NO_ID){
            $serialized[Usuario_model::ID_COLUMN] = $this->id;
        }
        $serialized[Usuario_model::NOME_COLUMN] = $this->nome;
        $serialized[Usuario_model::EMAIL_COLUMN] = $this->email;
        $serialized[Usuario_model::SENHA_COLUMN] = $this->senha;
        $serialized[Usuario_model::TIPO_USUARIO_COLUMN] = $this->tipo_usuario;
        $serialized[Usuario_model::IMAGEM_COLUMN] = $this->imagem->getID();
    }  
}

?>