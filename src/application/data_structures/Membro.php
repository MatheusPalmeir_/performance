<?php 

class Membro{
    private $id;
    private $nome;
    private $cargo;
    private $imagem;
    const NO_ID = -1;

    public function __construct(){
        $args = func_get_args();
        switch (func_num_args()) {
            case 3:
                self::__construct1($args[0], $args[1], $args[2]);
                break;
            case 4:
                self::__construct2($args[0], $args[1], $args[2], $args[3]);
                break;
        }
    }

    public function __construct1(string $nome, string $cargo, Imagem $imagem){
        $this->id = self::NO_ID;
        $this->nome = $nome;
        $this->cargo = $cargo;
        $this->imagem = $imagem;
    }

    public function __construct2(int $id, 
                                 string $nome, 
                                 string $cargo, 
                                 Imagem $imagem){
        $this->id = $id;
        $this->nome = $nome;
        $this->cargo = $cargo;
        $this->imagem = $imagem;
    }

    public function getID(){ return $this->id; }
    public function getNome(){ return $this->nome; }
    public function getCargo(){ return $this->cargo; }
    public function getImagem(){ return $this->imagem; } 
}

?>