<?php 
/**
 * @param   $slug Em alguma classe superior deve haver algum
 *                método para preenchimento do slug
 *
 * @param   $imagens Noticia possui um array de imagens, variando de 0 a N
 */



class Noticia{
    private $id;
    private $titulo;
    private $descricao;
    private $data;
    private $slug; 
    private $imagens; 

    public function __construct(){
        $args = func_get_args();
        switch (func_num_args()) {
            case 5:
                self::__construct1($args[0], 
                                   $args[1], 
                                   $args[2], 
                                   $args[3], 
                                   $args[4]);
                break;
            case 6:
                self::__construct2($args[0],
                                  $args[1],
                                  $args[2],
                                  $args[3],
                                  $args[4],
                                  $args[5]);
                break;
        }
    }

    public function __construct1(string $titulo, 
                                 string $descricao, 
                                 string $data, 
                                 string $slug, 
                                 array  $imagens){
        $this->id = self::NO_ID;
        $this->titulo = $titulo;
        $this->data = $data;
        $this->descricao = $descricao;
        $this->slug = $slug;
        $this->imagens = $imagens;
    }

    public function __construct2(int    $id
                                string $titulo, 
                                string $descricao, 
                                string $data, 
                                string $slug, 
                                array  $imagens){
        $this->id = $id;
        $this->titulo = $titulo;
        $this->data = $data;
        $this->descricao = $descricao;
        $this->slug = $slug;
        $this->imagens = $imagens;
    }

    public function getID(){ return $this->id; }
    public function getTitulo(){ return $this->titulo; }
    public function getImagens(){ return $this->imagens; }
    public function getDescricao(){ return $this->descricao; }
    public function getSlug(){ return $this->slug; }
    public function getData(){ return $this->data; }
}

?>