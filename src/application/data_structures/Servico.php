<?php 

class Servico{
    private $id;
    private $nome;
    private $descricao;

    public function __construct(){
        $args = func_get_args();
        switch (func_num_args()) {
            case 2:
                self::__construct1($args[0], $args[1]);
                break;
            
            case 3:
                self::__construct2($args[0], $args[1], $args[2]);
                break;
        }
    }

    public function __construct1(string $nome, string $descricao){
        $this->id = self::NO_ID;
        $this->nome = $nome;
        $this->descricao = $descricao;
    }

    public function __construct2($id, string $nome, string $descricao){
        $this->id = $id;
        $this->nome = $nome;
        $this->descricao = $descricao;
    }

    public function getID(){ return $this->id; }
    public function getNome(){ return $this->nome; }
    public function getDescricao(){ return $this->descricao; }
}

?>