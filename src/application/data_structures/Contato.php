<?php 
require_once(APPPATH."models/Contato_model.php");

class Contato{
    private $id;
    private $nome;
    private $descricao;
    private $icone;
    const NO_ID = -1;

    public function __construct(){
        $args = func_get_args();
        switch(func_num_args()){
            case 3:
                self::__construct1($args[0], $args[1], $args[2]);
                break;
            case 4:
                self::__construct2($args[0], $args[1], $args[2], $args[3]);
                break;
        }
    }

    public function __construct1(string $nome, 
                                 string $descricao,
                                 string $icone){
        $this->id = self::NO_ID;
        $this->nome = $nome;
        $this->descricao = $descricao;
        $this->icone = $icone;
    }

    public function __construct2(int $id, 
                                 string $nome, 
                                 string $descricao,
                                 string $icone){
        $this->id = $id;
        $this->nome = $nome;
        $this->descricao = $descricao;
        $this->icone = $icone;
    }

    public function getID(){ return $this->id; }
    public function getNome(){ return $this->nome; }
    public function getDescricao(){ return $this->descricao; }
    public function getIcone(){ return $this->icone; }

    public function serialize(){
        $serialized = array();
        if($this->id != self::NO_ID){
            $serialized[Contato_model::ID_COLUMN] = $this->id;
        }
        $serialized[Contato_model::NOME_COLUMN] = $this->column;
        $serialized[Contato_model::DESCRICAO_COLUMN] = $this->descricao;
        $serialized[Contato_model::ICONE_COLUMN] = $this->icone;
        return $serialized;
    }

    public function __toString(){ return $this->getDescricao(); }
}

?>