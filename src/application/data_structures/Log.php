<?php 

class Log{
    private $id;
    private $tabela;
    private $operacao;
    private $usuario;
    private $ip;
    private $data_registro;
    const NO_ID = -1;

    public function __construct(){
        $args = func_get_args();
        switch(func_num_args()){
            case 4:
                self::__construct1($args[0],
                                   $args[1],
                                   $args[2],
                                   $args[3]);
                break;
            case 5:
                self::__construct1($args[0],
                                   $args[1],
                                   $args[2],
                                   $args[3],
                                   $args[4]);
                break;
        }
    }

    public function __construct1(string  $tabela, 
                                 string  $operacao, 
                                 Usuario $usuario, 
                                 string  $ip){
        $this->id = self::NO_ID;
        $this->tabela = $tabela;
        $this->operacao = $operacao;
        $this->usuario = $usuario;
        $this->ip = $ip;
    }

    public function __construct2(int     $id
                                 string  $tabela, 
                                 string  $operacao, 
                                 Usuario $usuario, 
                                 string  $ip){
        $this->id = $id;
        $this->tabela = $tabela;
        $this->operacao = $operacao;
        $this->usuario = $usuario;
        $this->ip = $ip;
    }

    public function getID(){ return $this->id; }
    public function getTabela(){ return $this->tabela; }
    public function getOperacao(){ return $this->operacao; }
    public function getUsuario(){ return $this->usuario; }
    public function getIP(){ return $this->ip; }
    public function getData(){ return $this->data_registro; }
}

?>