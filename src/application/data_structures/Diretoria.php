<?php 

class Diretoria{
    private $id;
    private $nome;
    private $descricao;
    private $membros;
    const NO_ID = -1;

    public function __construct(){
        $args = func_get_args();
        switch(func_num_args()){
            case 3:
                self::__construct1($args[0], $args[1], $args[2]);
                break;
            case 4:
                self::__construct2($args[0], $args[1], $args[2], $args[3]);
                break;
        }
    }

    public function __construct1(string $nome, 
                                 string $descricao, 
                                 array $membros){
        $this->id = self::NO_ID;
        $this->nome = $nome;
        $this->descricao = $descricao;
        $this->membros = $membros
    }

    public function __construct2(int    $id, 
                                 string $nome, 
                                 string $descricao, 
                                 array  $membros){
        $this->id = $id;
        $this->nome = $nome;
        $this->descricao = $descricao;
        $this->membros = $membros;
    }

    public function getID(){ return $this->id; }
    public function getNome(){ return $this->nome; }
    public function getDescricao(){ return $this->descricao; }

    /**
     * Retorna os membros pertencentes à diretoria
     * @return Array<Membro> Array de membros
     */
    public function getMembros(){ return $this->membros; }
}

?>