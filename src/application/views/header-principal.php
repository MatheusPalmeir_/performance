<!DOCTYPE html>
	<html lang="pt-br">
		<head>
		  <meta charset="utf-8">
		  <title>Performance Jr. | Principal</title>
		  <meta content="width=device-width, initial-scale=1.0" name="viewport">
		  <meta content="" name="keywords">
		  <meta content="" name="description">
		  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
		  <!-- Favicons -->
		  <link href="img/favicon.png" rel="icon">
		  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">
		
		  <!-- Google Fonts -->
		  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Poppins:300,400,500,700" rel="stylesheet">

		  <!-- Bootstrap CSS File -->
		  <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet">

		  <!-- Libraries CSS Files -->
		  <link href="<?php echo base_url('assets/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet">
		  <link href="<?php echo base_url('assets/css/animate.min.css'); ?>" rel="stylesheet">

		  <!-- Main Stylesheet File -->
		  <link href="<?php echo base_url('assets/css/styleViewPrincipal.css'); ?>" rel="stylesheet">

		  <!-- =======================================================
		    Theme Name: Regna
		    Theme URL: https://bootstrapmade.com/regna-bootstrap-onepage-template/
		    Author: BootstrapMade.com
		    License: https://bootstrapmade.com/license/
		  ======================================================= -->
		</head>
		<body>