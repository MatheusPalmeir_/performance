  <!--==========================
        Footer
      ============================-->
      <footer id="footer">
        <div class="footer-top">
          <div class="container">

          </div>
        </div>

        <div class="container">
          <div class="copyright">
            &copy; <strong>Performance Jr.</strong> | Todos os Direitos Reservados
          </div>
        </div>
      </footer><!-- #footer -->

      <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

      <!-- JavaScript Libraries -->
      <script src="assets/js/jquery-3.3.1.js"></script>
      <script src="assets/js/jquery-migrate.js"></script>
      <script src="assets/js/bootstrap.bundle.js"></script>
      <script src="assets/easing/easing.min.js"></script>
      <script src="assets/js/wow.min.js"></script>
      <script src="assets/js/waypoints.min.js"></script>
      <script src="assets/js/counterup.min.js"></script>
      <script src="assets/js/hoverIntent.js"></script>
      <script src="assets/js/superfish.min.js"></script>

      <!-- Contact Form JavaScript File -->
      <script src="assets/contactform/contactform.js"></script>

      <!-- Template Main Javascript File -->
      <script src="assets/js/viewPrincipal.js"></script>

  </body>
</html>
