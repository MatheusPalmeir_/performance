<div id="homebody">
	<div class="page-header">

		<h1><?= $verbose_name ?> <small>listar</small></h1>
		<!-- <a href="<?= base_url() ?>" class="btn btn-info"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Voltar</a> -->
	</div>
	<div class="message">
	<?php
	if($this->session->flashdata('success') == TRUE){
		echo '<div class="alert alert-success alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Sucesso!</strong> Operação realizada com sucesso! </div>'; 
	}
	?>
	</div>

	<div class="alinhado-centro borda-base espaco-vertical">
		<a href="<?= base_url($controller.'/create/') ?>" class="btn btn-info"><span class='glyphicon glyphicon-plus-sign' aria-hidden='true'></span> Novo</a>
		<br><br><br>

		<?php $busca = array("name"=>"form-search","class"=>"");
		echo form_open(base_url($controller.'/index/'),$busca);
		echo form_input(array("type"=>"text","name"=>"search", "class"=>"col-md-9"));
		echo form_input(array("type"=>"submit","class"=>"btn btn-info","name"=>"btn-search","value"=>"Buscar"));
		echo form_close();?>	
	</div>
	<div class="">
		<table class="<?= $table['class'] ?>">
			<thead>
				<tr>
					
					<?php 
					for($i = 0 ; $i < count($table['header']); $i++){
						echo '<th>'.$table['header'][$i].'</th>';
					}
					?>
					<th>Ver</th>
				</tr>
			</thead>
			<tbody>

				<?php

				for($i = 0 ; $i < count($objects); $i++){
					echo '<tr>';
					for($j = 0 ; $j < count($table['body']); $j++){
					
					echo '<td>'.$objects[$i][$table['body'][$j]].'</td>';
					
					}
					echo '<td><a class="btn btn-info" href="'.base_url($controller.'/detail/'.$objects[$i]['id']).'">Ver</a></td>';
					echo '</tr>';
				}

				?>
			</tbody>
		</table>
		<?php echo $pagination; ?>
	</div>
</div>
</div>