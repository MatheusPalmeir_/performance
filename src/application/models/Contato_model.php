<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH."models/My_model.php");
require_once(APPPATH."data_structures/Contato.php");

class Contato_model extends My_model{
    const DB_TABLE         = "contato";
    const ID_COLUMN        = "id";
    const NOME_COLUMN      = "nome";
    const DESCRICAO_COLUMN = "descricao";
    const ICONE_COLUMN     = "icone";

    public function __construct(){
        parent::__construct();
    }

    public function get(int $id){
        $this->db->where(self::ID_COLUMN, $id);
        $row = $this->db->get(self::DB_TABLE)->row_array();
        return new Contato($row[self::ID_COLUMN],
                           $row[self::NOME_COLUMN],
                           $row[self::DESCRICAO_COLUMN],
                           $row[self::ICONE_COLUMN]);
    }

    public function insert(Contato $contato){
        $this->db->insert(self::DB_TABLE, $contato->serialize());
        if($this->successQuery()){
            $this->log(self::DB_TABLE, Log::INSERT);
            return true;
        }
        return false;
    }

    public function update(Contato $contato){
        $this->db->where(self::ID_COLUMN, $contato->getID());
        $this->db->update(self::DB_TABLE, $contato->serialize());
        if($this->successQuery()){
            $this->log(self::DB_TABLE, Log::UPDATE);
            return true;
        }
        return false;
    }

    public function remove(Contato $contato){
        $this->db->where(self::ID_COLUMN, $contato->id);
        $this->db->delete(self::DB_TABLE);
        if($this->successQuery()){
            $this->log(self::DB_TABLE, Log::DELETE);
            return true;
        }
        return false;
    }

    public function list(){
        $result = $this->db->get(self::DB_TABLE)->result_array();
        $contatos = array();
        foreach ($result as $key => $contato) {
            $contatos[] = new Contato($contato[self::ID_COLUMN],
                                      $contato[self::NOME_COLUMN],
                                      $contato[self::DESCRICAO_COLUMN],
                                      $contato[self::ICONE_COLUMN]);
        }

        return $contatos;
    }
    


}