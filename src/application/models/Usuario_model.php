<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH."models/My_model.php");
require_once(APPPATH."data_structures/Usuario.php");

class Usuario_model extends My_model{
    const DB_TABLE            = "usuario";
    const ID_COLUMN           = "id";
    const NOME_COLUMN         = "nome";
    const EMAIL_COLUMN        = "email";
    const SENHA_COLUMN        = "senha";
    const TIPO_USUARIO_COLUMN = "tipo_usuario";
    const IMAGEM_COLUMN       = "imagem_id";

    public function __construct(){
        parent::__construct();
        $this->load->model("Image_model", "imageModel");
    }

    public function get(int $id){
        $this->db->where(self::ID_COLUMN, $id);
        $row = $this->db->get(self::DB_TABLE)->row_array();
        $image = $this->imageModel->get($row[self::IMAGEM_COLUMN]);
        return new Usuario($row[self::ID_COLUMN],
                           $row[self::EMAIL_COLUMN],
                           $row[self::SENHA_COLUMN],
                           $row[self::NOME_COLUMN],
                           "",
                           $row[self::TIPO_USUARIO_COLUMN],
                           $image
                           );
    }

    public function getByEmail(string $email){
        $this->db->where(self::EMAIL_COLUMN, $email);
        $row = $this->db->get(self::DB_TABLE)->row_array();
        $image = $this->imageModel->get($row[self::IMAGEM_COLUMN]);
        return new Usuario($row[self::ID_COLUMN],
                           $row[self::EMAIL_COLUMN],
                           $row[self::SENHA_COLUMN],
                           $row[self::NOME_COLUMN],
                           "",
                           $row[self::TIPO_USUARIO_COLUMN],
                           $image
                           );
    }

    public function insert(Usuario $usuario){
        $this->db->insert(self::DB_TABLE, $usuario->serializeSQL());
        if($this->successQuery()){
            $this->log(self::DB_TABLE, Log::INSERT);
            return true;
        }
        return false;
    }

    public function update(Usuario $usuario){
        $this->db->where(self::ID_COLUMN, $usuario->getID());
        $this->db->update(self::DB_TABLE, $usuario->serializeSQL());
        if($this->successQuery()){
            $this->log(self::DB_TABLE, Log::UPDATE);
            return true;
        }
        return false;
    }

    public function remove(Usuario $usuario){
        $this->db->where(self::ID_COLUMN, $usuario->id);
        $this->db->delete(self::DB_TABLE);
        if($this->successQuery()){
            $this->log(self::DB_TABLE, Log::DELETE);
            return true;
        }
        return false;
    }

    public function list(){
        $this->db = $this->imageModel->joinTable(self::DB_TABLE, $this->db);
        $result = $this->db->get(self::DB_TABLE)->result_array();
        $usuarios = array();
        foreach ($result as $key => $usuario) {
            $image = new Imagem($usuario[Image_model::ID_COLUMN],
                                $usuario[Image_model::NAME_COLUMN]);
            $usuarios[] = new Usuario($usuario[self::ID_COLUMN],
                                      $usuario[self::EMAIL_COLUMN],
                                      $usuario[self::SENHA_COLUMN],
                                      $usuario[self::NOME_COLUMN],
                                      "",
                                      $usuario[self::TIPO_USUARIO_COLUMN],
                                      $image);
        }

        return $usuarios;
    }
    


}