<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH."data_structures/Imagem.php");


class ImageHandlingStrategy{
    protected $table;
    protected $tableImageIdColumnName;
    protected $imageTable;
    protected $idColumName;
    protected $imageColumnName;

    public function __construct(string $table,
                                string $tableImageIdColumnName,
                                string $imageTable,
                                string $idColumName,
                                string $imageColumnName){
        $this->table = $table;
        $this->tableImageIdColumnName = $tableImageIdColumnName;
        $this->imageTable = $imageTable;
        $this->idColumName = $idColumName;
        $this->imageColumnName = $imageColumnName;
    }
}

class OneToNImageHandlingStrategy extends ImageHandlingStrategy{

    public function getImages($id){     
        $this->db->join(Image_model::DB_TABLE,
                        "imagem.id = $this->imageColumnName", 
                        'INNER');
        $this->db->where("$this->imageTable.$this->idColumName", $id);
        $result = $this->db->get($this->imageTable)->result();
        if(!$result){
            return array(new Image(1, 'default.jpg'));
        }

        $images = array();
        foreach ($result as $key => $value) {
            $images[] = new Image($value['id'], $value['nome']);
        }

        return $images;
    }

    public function insertImages($id, $images){
        $CI = &get_instance();
        $CI->load->model("My_Model", "model");
        foreach ($images as $key => $image) {
            $CI->db->insert(
                $this->imageTable,
                array(
                    $this->idColumName => $id,
                    $this->imageColumnName => $image->getID()
                ) 
            );
            if(!$CI->model->successQuery()){
                return false;
            }else{
                $CI->model->log($this->imageTable, Log::INSERT);
            }
        }
        $CI->model->Log($this->imageTable, Log::INSERT);
    }
}

class OneToOneImageHandlingStrategy extends ImageHandlingStrategy{
    public function join($db){
        $db->join(Image_model::DB_TABLE, 
                  Image_model::DB_TABLE.".".Image_model::ID_COLUMN."=$this->tableImageIdColumnName", "INNER");
        return $db; 
    }
}

require_once(APPPATH."models/My_Model.php");
class Image_model extends My_Model{
    const DB_TABLE = "imagem";
    const ID_COLUMN = "id";
    const NAME_COLUMN = "nome";
    public function __construct(){
        parent::__construct();
    }

    private function SelectStrategy($table){
        if($table == Imagem::PROJETO){
            //TODO:require_once(APPPATH."models/Projeto_model.php");
            $Strategy = new OneToNImageHandlingStrategy(
                                Imagem::PROJETO,
                                "id",
                                Imagem::PROJETO."_imagem",
                                "id_projeto",
                                "id_imagem"
                                );
        }else if($table == Imagem::NOTICIA){
            $Strategy = new OneToNImageHandlingStrategy(
                                Imagem::NOTICIA,
                                "id",
                                Imagem::NOTICIA."_imagem",
                                "noticia_id",
                                "imagem_id"
                                );
        }else{
            require_once(APPPATH."models/Usuario_model.php");
            if($table == Usuario_model::DB_TABLE){
                $Strategy = new OneToOneImageHandlingStrategy(
                                    Usuario_model::DB_TABLE,
                                    Usuario_model::IMAGEM_COLUMN,
                                    "",
                                    "",
                                    ""
                                );
            }else{
                return false;
            }
        }
        
        return $Strategy;
    }

    public function get($id){
        $this->db->where(self::ID_COLUMN, $id);
        $result = $this->db->get(self::DB_TABLE)->row_array();
        return new Imagem($result[self::ID_COLUMN], 
                         $result[self::NAME_COLUMN]);
    }

    public function getImages($table, $id){
        $Strategy = $this->SelectStrategy($table);
        return $Strategy->getImages($id);
    }

    public function insert($image){
        $this->db->insert( 
            Image_model::DB_TABLE,
            array(
                Image_model::NAME_COLUMN => $image->getName()
            )
        );
        if($this->successQuery()){
            $id = $this->db->insert_id();
            $this->log(Image_model::DB_TABLE, Log::INSERT);
            return $id;
        }
        return false;
    }

    public function insertImages($table, $id, $images){
        if(!is_array($images)){
            $images = array($images);
        }
        foreach ($images as $key => $image) {
            $images[$key]->setID($this->insert($image));
        }
        $Strategy = $this->SelectStrategy($table);
        return $Strategy->insertImages($id, $images);
    }

    public function delete($image){
        if($image->getID() == Imagem::NO_ID){
            return false;
        }

        $this->db->where(Image_model::ID_COLUMN, $image->getID());
        $this->db->delete(Image_model::DB_TABLE);
        if($this->successQuery()){
            $this->log(Image_model::DB_TABLE, Log::DELETE);
            return true;
        }
        return false;
    }

    public function deleteImages($images){
        foreach ($images as $key => $image) {
            if(!$this->delete($image)){
                return false;
            }
        }
        return true;
    }

    public function list(){
        $result = $this->db->get(Image_model::DB_TABLE)->result_array();
        $images = array();
        foreach ($result as $key => $value) {
            $images[] = new Imagem($value[Image_model::ID_COLUMN],
                                  $value[Image_model::NAME_COLUMN]);
        }

        return $images;
    }

    public function joinTable(string $Table, $db){
        $Strategy = $this->SelectStrategy($Table);
        return $Strategy->join($db);
    }
}