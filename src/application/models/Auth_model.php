<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH."data_structures/Usuario.php");

class Auth_model extends CI_Model{

	public function __construct(){
		parent::__construct();
		new Usuario();
	}

	public function getPasswordHash($email){
		$this->db->where('email', $email);
   		$usuario = $this->db->get('usuario')->row();
   		return ($usuario!= NULL) ? $usuario->senha : false;
	}

	public function getUsuario($email){
		$this->load->model("Usuario_model", "usuarioModel");
		$this->db->join('imagem', 'usuario.imagem_id = imagem.id', 'INNER');
		$this->db->where('email', $email);
		$this->db->select('usuario.id, usuario.nome, email, tipo_usuario, imagem.nome as imagem');
   		$usuario = $this->db->get('usuario')->row();
   		return $this->usuarioModel->getByEmail($email);
	}

}