<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller{

	public function __construct(){
		parent::__construct();
		
	}

	public function index(){
        $this->load->view('header-principal');
        $this->load->view('index-view-principal');
        $this->load->view('footer-view-principal');
    }

}

?>