<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'controllers/AuthLog.php');

/**
 * Classe responsável por gerenciar as ações de um administrador
 */
class Admin extends AuthLog {
    public function __construct(){
        parent::__construct();
        $this->load->helper('frontend_helper'); 
        $this->load->model('Usuario_model', 'usuarioModel');
        $this->load->model('Image_model', 'imageModel');
    }

    /**
     * Função que retorna o HTML padrão do controller
     */
    public function index(){
        
        $this->load->view('html-header-admin');
        $this->load->view('header-admin');
        $this->load->view('home');
        $this->load->view('footer-admin');
        $this->imageModel->insertImages(Imagem::PROJETO, 1,
                                   array(
                                       new Imagem("OI.txt")
                                   ));
        //print_r($this->usuarioModel->list());
        //print_r($this->imageModel->list());
    }

    private function crypt ($password){
        $options = ['cost' => 12];
        $password = password_hash($password, PASSWORD_DEFAULT, $options);
        return $password; 
    }

    public function contatos(){
        $this->load->model('contato_model', 'contato');
        $data['contatos'] = $this->contato->list();
        $this->load->view('html-header-admin');
        $this->load->view('header-admin');
        $this->load->view('contato/listar', $data);
        $this->load->view('footer-admin');
    }
}